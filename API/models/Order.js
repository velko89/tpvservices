const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    name: String,
    price: Number,
    discount: Number,
    table_id: Number,
    amount: Number,
    state: String,
    paid: {
        type: String,
        default: 'false'
    }, 
    destination: String,
    image: String,
    finished_timestamp: String,
    ticket_id: String
});

// OrderSchema.index({name: 1, table_id: 1}, { unique: true });

OrderSchema.post('save',  (doc) => {
    require('../socket').req.send(JSON.stringify({
        changed: 'order',
        destination: doc._doc.destination,
        state: doc._doc.state,
        action: 'save',
        table_id: doc._doc.table_id
    }));
});

OrderSchema.post('updateMany', function (doc) {
    require('../socket').req.send(JSON.stringify({
        action: 'updateMany',
        changed: 'order',
        updates: this.getUpdate(),
        conditions: this._conditions
    }));
});

OrderSchema.post('findOneAndUpdate',  (doc) => {
    console.dir(doc);
    require('../socket').req.send(JSON.stringify({
        action: 'findOneAndUpdate',
        changed: 'order',
        destination: doc._doc.destination,
    }));
});

OrderSchema.post('deleteMany',  () => {
    require('../socket').req.send(JSON.stringify({
        action: 'deleteMany',
        changed: 'order',
    }));
});

OrderSchema.post('deleteOne',  () => {
    require('../socket').req.send(JSON.stringify({
        action: 'deleteOne',
        changed: 'order'
    }));
});

OrderSchema.statics.hasOpenedBilles = function () {
    return new Promise((resolve, reject) => {
        this.find({}).then(orders => {
            resolve(orders.length > 0);
        });
    });
}

OrderSchema.statics.getTableOrders = function (tableId, allUnpaid) {
    return new Promise((resolve, reject) => {
        let searchFilter = {'table_id': tableId, paid: 'false'};

        if (allUnpaid == 'true') {
            searchFilter = {'table_id': tableId, paid: { $in: ['false', 'pending'] }};
        }
        
        this.find(searchFilter)
            .then(response => {
                console.dir('2->');
                let finalBill = 0.0;
    
                response.forEach(element => {
                    finalBill += element.price * element.amount;
                });
                
                let responseData = {
                    total: finalBill,
                    orders: response,
                    table_id: tableId
                };
    
                resolve(responseData);
            })
            .catch(e => {
                reject(e);
            });
    });
}

module.exports = mongoose.model('orders', OrderSchema);
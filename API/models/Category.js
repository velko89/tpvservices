const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true
    },
    type: String
});

module.exports = mongoose.model('categories', CategorySchema);
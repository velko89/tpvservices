const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TicketSchema = new Schema({
    ticket_id: {
        type: String,
        unique: true
    },
    total: Number,
    paidAmount: Number
});

module.exports = mongoose.model('tickets', TicketSchema);
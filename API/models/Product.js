const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true
    },
    price: Number,
    image: String,
    description: String,
    groups: Array,
    destionation: String
});

module.exports = mongoose.model('products', ProductSchema);
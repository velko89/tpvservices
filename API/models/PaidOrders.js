const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaidOrdersSchema = new Schema({
    name: String,
    price: Number,
    image: String,
    description: String,
    groups: Array,
    destionation: String,
    cash_session_id: String,
    paid_timestamp: String,
    amount: Number,
    confirmed: Boolean,
    ticket_id: String,
    cash_flow_id: Schema.Types.ObjectId,
});

PaidOrdersSchema.post('save', notifyChange);
PaidOrdersSchema.post('deleteMany', notifyChange);

function notifyChange () {
    require('../socket').req.send(JSON.stringify({
        changed: 'order',
    }));

    require('../socket').req.send(JSON.stringify({
        changed: 'temp-order',
    }));

    require('../socket').req.send(JSON.stringify({
        changed: 'paid-order',
    }));
}

module.exports = mongoose.model('paid_orders', PaidOrdersSchema);
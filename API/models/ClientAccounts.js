const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientAccountsSchema = new Schema({
    tag_id: String,
    discount: {
        bill: {
            amount: Number, 
            left: Number,
        }
    },
    balance: {
        type: Number,
        default: 0.0
    },
});


module.exports = mongoose.model('client_accounts', ClientAccountsSchema);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TableDataSchema = new Schema({
    name: {
        type: String,
        index: true,
        unique: true
    },

    location: {
        mapId: Number,
        x: Number,
        y: Number
    },

    needHelp: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('table_data', TableDataSchema);
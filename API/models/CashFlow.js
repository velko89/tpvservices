const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PaidOrders = require('./PaidOrders');

const CashFlowSchema = new Schema({
    cash_flow_opening_timestamp: String,
    cash_flow_closing_timestamp: String,
    state: String,
    opening_money: Number,
    closing_money: Number
});

CashFlowSchema.post('updateOne',  (doc) => {
    require('../socket').req.send(JSON.stringify({
        changed: 'cash-flow',
    }));
});

CashFlowSchema.post('save',  (doc) => {
    require('../socket').req.send(JSON.stringify({
        changed: 'cash-flow',
    }));
});

CashFlowSchema.statics.getOpenId = function () {
    return new Promise((resolve, reject) => {
        this.findOne({'state': 'Opened'}).then(res => {
            if (res) {
                resolve(res._id);
            } else {
                reject();
            }
        }).catch(() => {
            reject();
        });
    });
};


CashFlowSchema.statics.getActiveCashFlow = function () {
    return new Promise((resolve, reject) => {
        this.findOne({'state': 'Opened'}).then(res => {
            if (res) {
                resolve(res);
            } else {
                reject();
            }
        }).catch(() => {
            reject();
        });
    });
};

CashFlowSchema.statics.getActiveCashFlowPaidBills = function () {
    return new Promise((resolve, reject) => {
        this.findOne({'state': 'Opened'}).then(res => {
            if (res) {
                PaidOrders.find({cash_flow_id: res._id}).then(paidBills => {
                    resolve(paidBills);
                });
            } else {
                reject();
            }
        }).catch(() => {
            reject();
        });
    });
};

module.exports = mongoose.model('cash_flows', CashFlowSchema);
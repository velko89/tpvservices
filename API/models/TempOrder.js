const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TempOrderSchema = new Schema({
    name: String,
    price: Number,
    table_id: Number,
    amount: Number,
    destination: String,
    image: String
});

/**
 * Middlewares
 */
TempOrderSchema.post('save',  () => {
    require('../socket').req.send(JSON.stringify({
        changed: 'temp-order'
    }));
});

TempOrderSchema.post('remove',  () => {
    require('../socket').req.send(JSON.stringify({
        changed: 'temp-order'
    }));
});

TempOrderSchema.post('deleteMany',  () => {
    require('../socket').req.send(JSON.stringify({
        changed: 'temp-order'
    }));
});

TempOrderSchema.post('deleteOne',  () => {
    require('../socket').req.send(JSON.stringify({
        changed: 'temp-order'
    }));
});

TempOrderSchema.index({name: 1, table_id: 1}, { unique: true });

module.exports = mongoose.model('temp_order', TempOrderSchema);
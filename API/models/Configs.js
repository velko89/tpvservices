const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigsSchema = new Schema({
    config_type: String,
    name: String,
    icon: String,
    commerce: String,
    description: String,
    password: String
});

module.exports = mongoose.model('configs', ConfigsSchema);
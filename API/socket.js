'use strict';

const 
    zmq = require('zeromq'),
    req = zmq.socket('req');

req.connect('tcp://ics:3010');

module.exports = { req };

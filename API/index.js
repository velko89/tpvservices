const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');


require('dotenv/config');

const APP_PORT = process.env.WEB_API_CASINO_PORT || 3000;


app.use(express.json());

app.all('/*', function(req, res, next) {
    if (req.headers.origin != undefined) {
        res.setHeader("Access-Control-Allow-Origin", req.headers.origin);
    }
    
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

/**
 * Import routes
 */
const productsRoute = require('./routes/products');
const tempOrderRoute = require('./routes/tempOrder');
const categoriesRoute = require('./routes/categories');
const ordersRoute = require('./routes/order');
const payOrderRoute = require('./routes/payOrder');
const tablesRoute = require('./routes/tables');
const configRoute = require('./routes/configs');
const cashFlowsRoute = require('./routes/cashFlows')
const clientAccountRoute = require('./routes/clientAccount')
//const ticket = require('./routes/ticket')

app.use('/products', productsRoute);
app.use('/temp-order', tempOrderRoute);
app.use('/categories', categoriesRoute);
app.use('/orders', ordersRoute);
app.use('/pay-order', payOrderRoute);
app.use('/tables', tablesRoute);
app.use('/configs', configRoute);
app.use('/cash-flows', cashFlowsRoute);
app.use('/clients', clientAccountRoute);
//app.use('/ticket', ticket);

app.get('/', (req, res) => {
    res.send('{ok: 1}');
});

/**
 * Connect to Mongo DB
 */
//mongoose.connect(process.env.DB_CONN_STRING, 
mongoose.connect(`mongodb://mongodb:27017/tpv` , 
    {useNewUrlParser: true}, 
    () => console.log('Connected to MongoDB') );

app.listen(APP_PORT, () => console.log(`Listening on port ${APP_PORT}...`));

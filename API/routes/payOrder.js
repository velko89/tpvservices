const express = require('express');
const router = express.Router();
const PaidOrders = require('../models/PaidOrders');
const Order = require('../models/Order');
const TempOrder = require('../models/TempOrder');
const PayClientBill = require('./payClientBill');

router.get('/pending-tables', async (req, res) => {
    Order.find({paid: 'pending'}).distinct('table_id').exec().then(result => {
        res.json(result);
    });
});

router.get('/:allunpaid?', async (req, res) => {
    let allUnpaid = req.params['allunpaid'];

    Order.getTableOrders(parseInt(req.query['table_id']), allUnpaid).then(orders => {
        res.json(orders);
    }).catch(error => {
        res.status(500).json(error);
    });
});

router.post('/:close?', async (req, res) => {
    console.dir('-2');

    PayClientBill(req, res);
});

module.exports = router;

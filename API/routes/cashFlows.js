const express = require("express");
const router = express.Router();
const CashFlow = require("../models/CashFlow");
const Order = require("../models/Order");
const PaidOrders = require("../models/PaidOrders");

router.post("/", async (req, res) => {
  CashFlow.find({state: 'Opened'}).exec().then(result => {
    if (result.length <= 0) {
      try {
        const cashFlow = new CashFlow({
          cash_flow_opening_timestamp: Date.now(),
          state: "Opened",
          opening_money: req.body.opening_money
        });
    
        cashFlow
          .save()
          .then(data => {
            res.json(data);
          })
          .catch(err => {
            res.statusCode = 402;
            res.json({ message: err });
          });
      } catch (err) {
        res.status(500).json({ errorCode: 0 });
      }
    } else {
      res.status(500).json({ errorCode: 554, message: 'There is an open session.' });
    }
  });

  
});


router.put('/', async (req, res) => {
  CashFlow.getActiveCashFlow().then(async result => {
    Order.hasOpenedBilles().then(activeBills => {
      console.dir('tuka ' + activeBills)
      if (activeBills) {
        /**
         * There are no open session to close
         */
          res.status(500).json({errorCode: 398});
          return;
      } else {
        let cashFlowId = result._id.toString() || null;
        /**
         * Get all bill amounts in the session
         */
        let totalBills = 0.0;
        CashFlow.getActiveCashFlowPaidBills().then(paidOrders => {
          console.dir('8->' + paidOrders);

          for (let i = 0; i < paidOrders.length; i++) {
            totalBills += paidOrders[i].price * (paidOrders[i].amount || 1);
            console.log(paidOrders[i].price + '*' + (paidOrders[i].amount || 1));
          }
      
          let openMoney = result.opening_money;
          let closeMoney = req.body.closing_money;
          let diffMoney = closeMoney - openMoney;
        
          result.updateOne({
            state: 'Closed',        
            cash_flow_closing_timestamp: Date.now(),
            closing_money: req.body.closing_money
          }).exec().then(result => {
            res.json({
              'closing_money': req.body.closing_money, 
              status: 'ok', 
              diffMoney, 
              totalBills,
              paidOrders,
              íd: cashFlowId || '0'
            });
          });
        });
      }
    });   
  });
});


router.get('/', async (req, res) => {

  CashFlow.findOne({state: 'Opened'}).exec().then(result => {
    if (result == null) {
      res.status(404).json({errorCode: 404});

    } else {
      res.json({status: 'ok', result});
    }

  });

});




module.exports = router;

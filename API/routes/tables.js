const express = require('express');
const router = express.Router();
const TableData = require('../models/TableData');

router.get('/', async (req, res) => {
    let tables = await TableData.find({}).exec()
            .then(response => {
                res.json(response);
            });
});

router.post('/', async (req, res) => {
    let newTable = new TableData({
        name: req.body.name,
    
        location: {
            mapId: req.body.mapId,
            x: req.body.x,
            y: req.body.y
        },
    
        needHelp: false
    });

    await newTable.save().then(() => {
        res.json({ok: true});
    });
});

module.exports = router;

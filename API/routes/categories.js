const express = require('express');
const router = express.Router();
const Category = require('../models/Category');

router.get('/', async (req, res) => {
    try {
        const categories = await Category.find().exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            console.log('Error getting the posts');
        });
        
    } catch (err) {

    }
});

router.get('/:name', async (req, res) => {
    try {
        const categories = await Category.findOne({name: req.params['name']}).exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            console.log('Error getting the posts');
        });
        
    } catch (err) {

    }
});

module.exports = router;
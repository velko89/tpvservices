const express = require('express');
const router = express.Router();
const ClientAccounts = require('../models/ClientAccounts');
const PayClientBill = require('./payClientBill');
const Order = require('../models/Order');


router.post('/', async (req, res) => {
    let id = req.body.tag_id;

    let client = ClientAccounts.findOne({tag_id: id}).exec()
    .then(response => {
        res.json(response);
    })
    .catch(err => {
        res.status(500).json({'error': 1245, 'message': err});
    });
});

router.post('/update', async (req, res) => {
    let id = req.body.tag_id;

    let clientData = {
        balance: req.body.balance,
        discount: req.body.discount,
    };

    console.dir('7->' + clientData);
    
    ClientAccounts.findOneAndUpdate({tag_id: id}, clientData,  { upsert: true, new: true }).exec()
        .then(response => {
            res.json({'ok': true});
        })
        .catch(err => {
            res.status(500).json({'errorCode': 1273, err});
        });

});

router.post('/recharge', async (req, res) => {
    let id = req.body.tag_id;
    let amount = req.body.amount;

    let client = ClientAccounts.findOneAndUpdate({tag_id: id}, {$inc: {balance: amount}},  { upsert: true, new: true }).exec()
    .then(response => {
        res.json(response);
    })
    .catch(err => {
        res.status(500).json({'error': 1245, 'message': err});
    });
});

let getTableTotalBill = async (tableId) => {
    let totalToPay = 0;
    
    const order = await Order.find({'table_id': tableId, 'paid': { $ne: 'true' }}).exec();
    
    for (let i = 0; i < order.length; i++) {
        totalToPay += order[i].price * order[i].amount;
    }

    return totalToPay;
}

router.post('/pay-bill/:close?/:notpending?', async (req, res) => {

    let tableId = parseInt(req.body.table_id);
    let totalToPay = 0;
    let id = req.body.tag_id;

    const clientAccount = await ClientAccounts.findOne({tag_id: id}).exec();

    if (clientAccount && clientAccount.discount) {
        let tempTotalBill = await getTableTotalBill(tableId);

        if (clientAccount.discount.bill) {
            let tmpDiscount = tempTotalBill + (-1 * (tempTotalBill * parseFloat(clientAccount.discount.bill.amount)));
            
            if (clientAccount && clientAccount.balance >= tmpDiscount && (clientAccount.discount.bill.left > 0 || clientAccount.discount.bill.amount == -1)) {
                let discountProduct = new Order({
                    paid: false,
                    name: 'Descuento',
                    price: -1 * (tempTotalBill * parseFloat(clientAccount.discount.bill.amount)), 
                    table_id: tableId,
                    amount: 1
                });

                await ClientAccounts.updateOne({tag_id: id}, {$inc: {'discount.bill.left': -1} }).exec();
                await discountProduct.save();
            }
        }

    }

    const order = await Order.find({'table_id': tableId, 'paid': { $ne: 'true' }}).exec();
    

    for (let i = 0; i < order.length; i++) {
        totalToPay += order[i].price * order[i].amount;
    }

    if (clientAccount && clientAccount.balance >= totalToPay) {
        let resultClient = ClientAccounts.updateOne({tag_id: id}, {$inc: {balance: -totalToPay} }).exec().then(async resClient => {
            const clientUpdatedAccount = await ClientAccounts.findOne({tag_id: id}).exec();

            PayClientBill(req, res, clientUpdatedAccount);
        });
    } else {
        res.status(404).json({error: 'No hay saldo en esta cuenta.'});
    }
});

module.exports = router;

const PaidOrders = require('../models/PaidOrders');
const Order = require('../models/Order');
const TempOrder = require('../models/TempOrder');
const CashFlow = require('../models/CashFlow');
const Ticket = require('../models/Ticket');

let PayClientBill = async (req, res, additionalOkMessage = null) => {
    try {
        let tableId = parseInt(req.body.table_id);
        let received = parseFloat(req.body.received) || 0;
        console.dir('-1');
        
        if (req.params['close'] && req.params['close'] == 'true') {
            try {
                console.dir('0');
                MoveToPaid(true, received).then(data => {

                    // let query = { 'table_id': tableId, 'paid': { $in: ['pending'] } };
                    let query = { 'table_id': tableId };

                    if(req.params['notpending'] && req.params['notpending'] == 'true') {
                    }

                    Order.deleteMany(query).then(_ => {});
                    
                    res.json({'status': 'ok', 'service': 'pay', additionalOkMessage, data});                    
                }).catch(err => {
                    console.dir(err);
                    res.status(500).json({errorCode: 'a2'});
                });

            } catch (e) {
                console.dir('3->' + e);
                res.status(500).json({errorCode: 100});
            }
            
            return;
        } else {
            // Changing the 'paid' state to true
            // if the 'paid' state is set to true
            // and thare is some attempt to chage
            // the 'state' instead of changing it the product
            // will be removed from the table
            Order.updateMany({ 'table_id': tableId },  { 'paid': 'pending' }).exec();

            TempOrder.deleteMany({ 'table_id': tableId }).exec();
        }

        
        function MoveToPaid(confirmed, received = 0) {

            return new Promise((resolve, reject) => {
                Order.find({'table_id': tableId })
                .then(function(response) {
                    let payTime = Date.now();
                    let savePaidOrdersList = [];
    
                    CashFlow.getOpenId().then(cash_flow_id => {
                        let ticketId = '';
                        let total = 0;

                        for (let i = 0; i < response.length; i++) {
                            ticketId = response[i].ticket_id;

                            const orderToPay = new PaidOrders({
                                'name': response[i].name,
                                'price': response[i].price,
                                'table_id': response[i].table_id,
                                'amount': response[i].amount,
                                'destination': response[i].destination,
                                'image': response[i].image,
                                'paid_timestamp': payTime,
                                'ticket_id': response[i].ticket_id,
                                confirmed,
                                cash_flow_id
                            });
    
                            total += parseFloat(response[i].amount) * parseFloat(response[i].price);
                            savePaidOrdersList.push(orderToPay.save());
                        }
    
                        Promise.all(savePaidOrdersList).then(_ => {
                            let ticket = new Ticket({
                                ticket_id: ticketId,
                                total,
                                paidAmount: (received == 0) ? total : received
                            });

                            ticket.save().then(_ => {
                                // Remove all finished orders from the table
                                Order.deleteMany({ 'table_id': tableId, 
                                    'state': 'done', 
                                    paid: { $in: ['true'] } 
                                }).then(_ => {
                                    resolve({
                                        total,
                                        paidAmount: (received == 0) ? total : received,
                                        diff: (received == 0) ? 0 : received - total
                                    });
                                });
                            });
                        });
                    }).catch(err => {
                        console.dir(err);
                        res.status(500).json({errorCode: 'a2'});
                    });
                })
                .catch(function(error){
                    res.status(500).json({errorCode: 'a2'});
                });
            });
        }

        
    } catch (err) {
        res.status(500).json({errorCode: 'a1', err, 'a': req.body});
    }
}

module.exports = PayClientBill;
const express = require('express');
const router = express.Router();
const uuid = require('uuid');
const Order = require('../models/Order');

router.delete('/', async (req, res) => {
    Order.deleteOne({_id: req.query['id']}).exec()
        .then(_ => {
            res.json({status: 'ok'});
        })
        .catch(e => {
            res.status(500).json({status: 'error', err: e});
        });
});

router.put('/apply-discount', async (req, res) => {
    let discount = req.body.discount;
    let id = req.body.id;
    let table_id = req.body.table_id;
    let product;

    if (id != null) {
        product = await Order.findOne({'_id': id}).exec();
        let oldPrice = product.price;

        if (discount.includes('%')) {
            discount = parseFloat(discount.replace('%', ''));

            product.price = product.price - (product.price * (discount / 100));
        } else {
            product.price = product.price - parseFloat(discount);
        }

        if (!product.discount) {
            product.discount = oldPrice - product.price;
        } else {
            product.discount = product.discount + (oldPrice - product.price);
        }
    } else {
        product = new Order({
            paid: false,
            name: 'Descuento',
            price: -1 * parseFloat(discount), 
            table_id,
            amount: 1
        });
    }

    product.save().then(function(response) {
        res.json({status: 'ok'});
    })
    .catch(function(error){
        res.status(500).json({errorCode: 441, err: error});
    });
});

router.get('/', async (req, res) => {
    let filter = {};

    if (req.query['destination']) {
        filter['destination'] = req.query['destination'];
    }
    
    if (req.query['state']) {
        filter['state'] = req.query['state'];
    } else {
        filter['state'] = {
            $in: ['', null]
        };
    }

    if (req.query['table_id']) {
        filter['table_id'] = req.query['table_id'];
    }

    try {
        const order = await Order.find(filter).exec()
            .then(function(response) {
                res.json(response);
            })
            .catch(function(error){
                res.status(500).json({errorCode: 0});
            });
        
    } catch (err) {
        res.status(500).json({errorCode: 0});
    }
});

router.post('/direct', async(req, res) => {

    Order.findOne({'table_id': req.body.table_id}).then(async (activeOrder) => {
        ticket_id = uuid.v1();
        
        if (activeOrder) {
            ticket_id = activeOrder.ticket_id;
        } 

        const newOrder = new Order({
            'name': req.body.name,
            'price': req.body.price,
            'table_id': req.body.table_id,
            'amount': req.body.amount,
            'image': req.body.image,
            'destination': req.body.destination,
            ticket_id
        });
    
        newOrder.save().then(_ => {
            Order.getTableOrders(parseInt(req.body.table_id), true).then(orders => {
                res.json({status: 'ok', orders});
            });
        });
    });
});

router.put('/kitchen', async (req, res) => {
    let newState = req.body['state'] || 'doing';
    let filter = {
        state: newState
    }

    try {    
        /**
         * If the 'paid' state is set to false
         * change the state to the given one, if 
         * the 'paid' state is set to true and the new state is 'done', we remove 
         * the document from the collection
         */
        if (newState == 'done') {

            /* if(req.query['state'] == 'done') {
                filter['finished_timestamp'] = Date.now();
            } */
            filter['finished_timestamp'] = Date.now();

            Order.deleteOne({
                _id: req.body._id,
                paid: 'pending'
            }).exec().then(response => {
                res.json({ok: true});
            }).catch(e => {
                console.dir('4->' + e);
                res.status(500).json({errorCode: 123, errorMessage: e});
            });
        }
            
        const order = await Order.findOneAndUpdate({
            _id: req.body._id
        }, filter).exec()
            .then(function(response) {
                res.json({ok: true});
            }).catch(e => {
                console.dir('5->' + e);
                res.status(500).json({errorCode: 0, errorMessage: e});
            });
        
    } catch (e) {
        res.status(500).json({errorCode: 0, errorMessage: e});
    }
});

router.get('/temp-bill/:table', (req, res) => {
    Order.getTableOrders(req.params['table'], true).then(order => {
        res.json(order);
    });
});

router.put('/bartender', async (req, res) => {
    let newState = req.body['state'] || 'done';

    try {
        const order = await Order.updateMany({
            table_id: req.body['table_id']
        }, {state: newState}).exec()
            .then(function(response) {
                res.json({ok: true});
            }).catch(e => {
                console.dir('6->' + e);
                res.status(500).json({errorCode: 0, errorMessage: e});
            });
        
    } catch (e) {
        res.status(500).json({errorCode: 0, errorMessage: e});
    }
});


module.exports = router;

const express = require('express');
const router = express.Router();
const TempOrder = require('../models/TempOrder');
const Order = require('../models/Order');
const CashFlow = require('../models/CashFlow');
const uuid = require('uuid');

router.get('/', async (req, res) => {
    try {
        const tmpOrder = await TempOrder.find().exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            res.status(500).json({errorCode: 0});
        });
        
    } catch (err) {
        res.status(500).json({errorCode: 0});
    }
});

router.get('/:id', async (req, res) => {
    try {
        let tableId = parseInt(req.params['id']);
        const tmpOrder = await TempOrder.find({'table_id': tableId}).exec()
        .then(function(response) {
            console.log('1->' + response);
            res.json(response);
        })
        .catch(function(error){
            res.status(500).json({errorCode: 0});
        });
        
    } catch (err) {
        res.status(500).json({errorCode: 0});
    }
});

const responseJsonTableBill = async function (tableId, res) {
    try {
        const tmpOrder = TempOrder.find({'table_id': tableId}).exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            res.status(500).json({errorCode: 0});
        });
        
    } catch (err) {
        res.status(500).json({errorCode: 0});
    }
}

const responseJsonUpdateBill = async function (tableId, productName, res, req) {
    try {
        var order = await TempOrder.findOneAndUpdate({
            'table_id': tableId,
            'name': productName
        }, {
            'name': req.body.name,
            'price': req.body.price,
            'table_id': req.body.table_id,
            'amount': req.body.amount,
            'image': req.body.image,
            'destination': req.body.destination,
        }, function (err, doc) {
            if (!err) {
                responseJsonTableBill(req.body.table_id, res);
            }
        });

    } catch (error) {
        res.status(500).json({errorCode: 0});
    }
}

router.post('/', async (req, res) => {

    if (req.query['submit'] && req.query['submit'] == 1) {
        
        try {
            let tableId = parseInt(req.body.table_id);
            
            const tmpOrder = await TempOrder.find({'table_id': tableId}).exec()
            .then(async function(response) {
                
                Order.findOne({'table_id': tableId}).then(async (activeOrder) => {
                    ticket_id = uuid.v1();
                    
                    if (activeOrder) {
                        ticket_id = activeOrder.ticket_id;
                    } 

                    for (let i = 0; i < response.length; i++) {
                        const order = new Order({
                            'name': response[i].name,
                            'price': response[i].price,
                            'table_id': response[i].table_id,
                            'amount': response[i].amount,
                            'destination': response[i].destination,
                            'image': response[i].image,
                            'paid': 'false',
                            ticket_id
                        });
                        
                        order.save().then(data => {
                            TempOrder.deleteMany({'table_id': tableId, 'name': response[i].name}).exec();
                            res.json({
                                ok: true
                            });
                        })
                        .catch(async err => {
                            res.status(500).json({
                                errorCode: 121,
                                errorMessage: err
                            });
                        });
                    } 
                });
            })
            .catch(function(error){
                res.status(500).json({
                    errorCode: 0,
                    errorMessage: error
                });
            });
            
        } catch (err) {
            res.status(500).json({
                errorCode: 0,
                errorMessage: err
            });
        }
    } else {
        const tempOrder = new TempOrder({
            'name': req.body.name,
            'price': req.body.price,
            'table_id': req.body.table_id,
            'amount': req.body.amount,
            'image': req.body.image,
            'destination': req.body.destination,
        });
    

        tempOrder.save().then(data => {
            console.dir('asdads12313');
            responseJsonTableBill(req.body.table_id, res);
        })
        .catch(async err => {
            console.dir('asdads12313@@@@@');
            if(err.code == '11000') {
                responseJsonUpdateBill(req.body.table_id, req.body.name, res, req);
            }
        });
    }
});

router.put('/', async (req, res) => {
    responseJsonUpdateBill(req.body.table_id, req.body.name, res, req);
});

router.delete('/:name/:table', async (req, res) => {
    const tmpOrder = TempOrder.find({'table_id': req.params['table'], 'name': req.params['name']})
        .deleteOne()
        .then(() => {
            res.json({ok: true});
        });
});

module.exports = router;

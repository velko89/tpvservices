const express = require('express');
const router = express.Router();
const Product = require('../models/Product');

router.get('/', async (req, res) => {
    
    const searchQuery = {};

    if (req.query['category']) {
        searchQuery['category'] = req.query['category'];
    }

    try {
        const products = await Product.find(searchQuery).exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            console.log('Error getting the posts');
        });
        
    } catch (err) {

    }
});

router.get('/:name', async (req, res) => {
    try {
        let productName = req.params['name'];

        const products = await Product.find({'name': productName}).exec()
        .then(function(response) {
            res.json(response);
        })
        .catch(function(error){
            res.json(
                {
                    error: 'Can not find ' + productName
                }
            );
            console.log('Error getting the posts');
        });
        
    } catch (err) {

    }
});

router.post('/', async (req, res) => {
    const product = new Product({
        'name': req.body.name,
        'price': req.body.price,
    });

    product.save().then(data => {
        res.json(data);
    })
    .catch(err => {
        res.statusCode = 402;
        res.json({ message: err });
    });
});

module.exports = router;
'use strict';

const 
    zmq = require('zeromq/v5-compat'),
    responder = zmq.socket('rep'),
//    arduino = require('./arduino'), 
    web = require('./web-socket');

responder.on('message', function (data) {
    console.log('data recived');
    console.dir(JSON.parse(data));

    web.send(JSON.stringify(JSON.parse(data)));

    responder.send(JSON.stringify({
        'status': true
    }));
});

/*arduino.ArduinoEvents.on('message', msg => {
    let cleanMsg = msg.trim();

    if (cleanMsg.startsWith('code:')) {
        web.send({
            type: 'arduino',
            action: 'rfid',
            message: cleanMsg.replace('code:', '')
        })
    }
});*/

web.receive(data => {
    console.dir('FROM WEB: ' + data);
    arduino.sendMessage(data);
});

responder.bind('tcp://*:3010', () => {
    console.log(`Listen for clients on port 3010`);
});

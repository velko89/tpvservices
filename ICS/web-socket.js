const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const events = require('events');

const evtEmmiter = new events.EventEmitter();

io.on('connection', (socket) => {
	console.log('user connected');

	socket.on('message', (message) => {
		console.log('Message: ' + message);
		evtEmmiter.emit('client', message);
	});

	socket.on('diconnect', () => {
		var index = clientsList.indexOf(socket);
		if (index !== -1) clientsList.splice(socket, 1);
	});
});

let send = function (message) {
    io.send(message);
};

let receive = function (callback) {
	evtEmmiter.on('client', message => {
		callback(message);
	})
};

http.listen(3020, () => {
	console.log('Web Socket server started on port 3020...');
});

module.exports = { send, receive };
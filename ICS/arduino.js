const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const port = new SerialPort('\\\\.\\COM10', { baudRate: 9600 }, false);
const parser = port.pipe(new Readline({ delimiter: '\n' }));
const EventEmitter = require('events');
const ArduinoEvents = new EventEmitter();

port.on("open", () => {
  console.log('serial port open');
});

parser.on('data', data =>{
  if (!String(data).includes('no data')) {
    ArduinoEvents.emit('message', data);
  }
});


let sendMessage = (message) => {
    port.write(message, (err) => {
        if (err) {
            console.log('[ARDUINO MESSAGE SEND ERROR]: ', err.message);
        }
    });
};

module.exports = { sendMessage, ArduinoEvents };
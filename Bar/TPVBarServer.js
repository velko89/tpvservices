const express = require('express');
const http = require('http');
const path = require('path');

const app = express();

const port = 7000;

app.use(express.static(path.join(__dirname) + '/dist/TPVBar'));

app.get('/*', (req, res) => res.sendFile(express.static(path.join(__dirname) + '/dist/TPVBar')));

const server = http.createServer(app);

server.listen(port, '0.0.0.0', () => console.log(`App running on: http://localhost:${port}`));

#!/bin/bash

cd ./API  
docker build -t velko89/tpv_api:v1 .
docker push velko89/tpv_api:v1

cd ../Bar  
docker build -t velko89/tpv_bar:v1 .
docker push velko89/tpv_bar:v1

cd ../Client  
docker build -t velko89/tpv_client:v1 .
docker push velko89/tpv_client

cd ../ICS
docker build -t velko89/tpv_ics:v2 .
docker push velko89/tpv_ics:v2
